# Online Bookshop
a simple webapp for buying books online

# Table of Contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Screenshots](#screenshots)
* [Features](#features)

# General info
My main purpose for making this project was practising developing an independent project from scratch. 
# Technologies
Java Servlet (version 1.8), JSP, HTML, CSS, Java Script, MySQL
# Setup
to run this project, use IDE or command line; use relational database software; server - Apache Tomcat; sql script can be found in WebContent/sql directory
# Screenshots
![](/images/index.png)
![](/images/bestsellers.png)
![](/images/advancedSearch.png)
![](/images/myWishList.png)
# Features 
 1. Displaying books in stock
 2. Displaying Top 5 according to the number of sold copies
 3. Sign in / Register
 4. For registered users wish list and the list of orders available
 5. Search books by different categories

